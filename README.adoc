= TSS main provisioning use case

Thomas' Setup System (TSS) ansible playbook for the main provisioning use case.

== Prerequisites

This playbook sets up and customizes my personal systems.
It is meant to be run locally on the system itself.

Therefore, you need to install ansible:

[source,bash]
----
$ sudo apt install ansible
----

Copy link:individual_vars.example.yml[] to individual_vars.yml and adapt the values to your needs.

== Usage

To run the whole playbook, invoke

[source,bash]
----
$ make reqs && make install
----

You will be prompted for your sudo password.

You can also pass arguments like so:

[source,bash]
----
$ make ARGS=--check install
----
